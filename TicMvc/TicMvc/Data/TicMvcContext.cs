﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicMvc.Models;

namespace TicMvc.Data
{
    public class TicMvcContext : DbContext
    {
        public TicMvcContext(DbContextOptions<TicMvcContext> options)
            : base(options)
        {
        }

        public DbSet<Player> Player { get; set; }
        public DbSet<Game> Game { get; set; }
    }
}
