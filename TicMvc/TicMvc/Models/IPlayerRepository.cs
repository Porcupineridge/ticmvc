﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicMvc.Models
{
    public interface IPlayerRepository
    {
        Player GetPlayer(int Id);
        Game GetGame(int Id);
        
    }
}
