﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicMvc.Models
{
    public class MockPlayerRepository : IPlayerRepository
    {
        private List<Player> _playerList;
        private Game _game;

        public MockPlayerRepository()
        {
            _playerList = new List<Player>()
            {
                new Player { Id=1, Name = "Bob"},
                new Player { Id=2, Name = "Jon"}
            };
            _game = new Game() { Id = 1 };
        }

        public Game GetGame(int Id)
        {
            return _game;
        }

        public Player GetPlayer(int Id)
        {
            return _playerList.FirstOrDefault(e => e.Id == Id);
        }
       
    }
}
