﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicMvc.Models
{
    public enum GameStatus
    {
        NotStarted,
        Ongoing,
        Ended
    }
}
