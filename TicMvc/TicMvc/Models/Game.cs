﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicMvc.Models
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Player Player { get; set; }
        public Player Opponent { get; set; }
        public GameStatus GameStatus { get; set; }
        private int FlipACoin()
        {
            Random rnd = new Random();
            int coin = rnd.Next(1, 3);
            if (coin == 1)
            {
                Console.WriteLine($"{Player.Name } begins");
                return coin;
            }
            else
            {
                Console.WriteLine($"{Opponent.Name} begins");
                return coin;
            }
        }
    }
}
