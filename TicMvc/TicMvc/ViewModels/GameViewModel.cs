﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicMvc.Models;

namespace TicMvc.ViewModels
{
    public class GameViewModel
    {
        public Player Player { get; set; }
        public Player Opponent { get; set; }
        public Game Game { get; set; }
    }
}
