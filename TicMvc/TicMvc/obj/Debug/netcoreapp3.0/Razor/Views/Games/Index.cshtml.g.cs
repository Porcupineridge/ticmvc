#pragma checksum "C:\Users\Jakob\Desktop\Test\Experiment-transportlead\TicMvc\TicMvc\Views\Games\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "62ddbb473d0a68abf87daf4088f2f7e8f8b409b0"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Games_Index), @"mvc.1.0.view", @"/Views/Games/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Jakob\Desktop\Test\Experiment-transportlead\TicMvc\TicMvc\Views\_ViewImports.cshtml"
using TicMvc;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Jakob\Desktop\Test\Experiment-transportlead\TicMvc\TicMvc\Views\_ViewImports.cshtml"
using TicMvc.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Jakob\Desktop\Test\Experiment-transportlead\TicMvc\TicMvc\Views\_ViewImports.cshtml"
using TicMvc.ViewModels;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"62ddbb473d0a68abf87daf4088f2f7e8f8b409b0", @"/Views/Games/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"84da7ad4c54bdfea2107f55e062cac9a3a55626a", @"/Views/_ViewImports.cshtml")]
    public class Views_Games_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<TicMvc.ViewModels.GameViewModel>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
            WriteLiteral(@"
<style>
       
        .ads {
            text-align: center;
            margin: .4vw 0;
            background: #F4CC70;
            padding: .4vw;
        }

        .content {
            margin: .4vw 0;
            /*background: lightblue;*/
            padding: .4vw;
        }

        .tic-panel {
            border: solid 2px black;
            margin: .4vw 0;
            background: black;
            padding: .4vw;
        }

        .tic-box {
            border: solid black 1px;
            font-size: 5vw;
            background: white;
            text-align: center;
            color: black;
        }

            .tic-box:hover {
                background: lightgrey;
            }
</style>

<h1>Game</h1>
<div>
    ");
#nullable restore
#line 45 "C:\Users\Jakob\Desktop\Test\Experiment-transportlead\TicMvc\TicMvc\Views\Games\Index.cshtml"
Write(Model.Player.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral(" vs ");
#nullable restore
#line 45 "C:\Users\Jakob\Desktop\Test\Experiment-transportlead\TicMvc\TicMvc\Views\Games\Index.cshtml"
                     Write(Model.Opponent.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</div>\r\n<p>Player make a move. Select a number between 1-9</p>\r\n\r\n\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "62ddbb473d0a68abf87daf4088f2f7e8f8b409b04853", async() => {
                WriteLiteral(@"
    <meta charset=""utf-8"">
    <title>Tic Tac Toe</title>
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">

    <link rel=""stylesheet"" href=""https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"" integrity=""sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"" crossorigin=""anonymous"">
    <link rel=""stylesheet"" href=""css/styles.css"">
    
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "62ddbb473d0a68abf87daf4088f2f7e8f8b409b06254", async() => {
                WriteLiteral("\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n");
                WriteLiteral(@"            <div class=""col-12 col-sm-6 content"">
                <div class=""container-fluid tic-container"">
                    <div class=""row tic-row"">
                        <div id=""one"" class=""col-4 tic-box"" onclick=""changeContent(1)""> 1 </div>
                        <div id=""two""class=""col-4 tic-box"" onclick=""changeContent(2)""> 2 </div>
                        <div id=""three""class=""col-4 tic-box"" onclick=""changeContent(3)""> 3 </div>
                    </div>
                    <div class=""row tic-row"">
                        <div id=""four"" class=""col-4 tic-box"" onclick=""changeContent(4)""> 4 </div>
                        <div id=""five"" class=""col-4 tic-box"" onclick=""changeContent(5)""> 5 </div>
                        <div id=""six"" class=""col-4 tic-box"" onclick=""changeContent(6)""> 6 </div>
                    </div>
                    <div class=""row tic-row"">
                        <div id=""seven"" class=""col-4 tic-box"" onclick=""changeContent(7)""> 7 </div>
                        <");
                WriteLiteral(@"div id=""eight"" class=""col-4 tic-box"" onclick=""changeContent(8)""> 8 </div>
                        <div id=""nine"" class=""col-4 tic-box"" onclick=""changeContent(9)""> 9 </div>
                    </div>
                </div>
            </div>
            



            
        </div>
    </div>
    <p id=""message""></p>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
<script>
    var count = 0;
    var turn = 0;
    function changeContent(x) {
        count++;
        if (turn == 0) {
            turn++;
            if (x == 1) 
                document.getElementById(""one"").innerHTML = ""X"";
            else if (x == 2)
                document.getElementById(""two"").innerHTML = ""X"";
            else if (x == 3)
                document.getElementById(""three"").innerHTML = ""X"";
            else if (x == 4)
                document.getElementById(""four"").innerHTML = ""X"";
            else if (x == 5)
                document.getElementById(""five"").innerHTML = ""X"";
            else if (x == 6)
                document.getElementById(""six"").innerHTML = ""X"";
            else if (x == 7)
                document.getElementById(""seven"").innerHTML = ""X"";
            else if (x == 8)
                document.getElementById(""eight"").innerHTML = ""X"";
            else if (x == 9)
                document.getElementById(""nine"").innerHTML = ""X"";
        }
    ");
            WriteLiteral(@"    else if (turn == 1) {
            turn--;
            if (x == 1) 
                document.getElementById(""one"").innerHTML = ""O"";
            else if (x == 2)
                document.getElementById(""two"").innerHTML = ""O"";
            else if (x == 3)
                document.getElementById(""three"").innerHTML = ""O"";
            else if (x == 4)
                document.getElementById(""four"").innerHTML = ""O"";
            else if (x == 5)
                document.getElementById(""five"").innerHTML = ""O"";
            else if (x == 6)
                document.getElementById(""six"").innerHTML = ""O"";
            else if (x == 7)
                document.getElementById(""seven"").innerHTML = ""O"";
            else if (x == 8)
                document.getElementById(""eight"").innerHTML = ""O"";
            else if (x == 9)
                document.getElementById(""nine"").innerHTML = ""O"";
        }
        

        if (count > 2)
            checkWin();

    }
    function checkWin() {

      ");
            WriteLiteral(@"  if (document.getElementById(""one"").innerHTML == 'X' && document.getElementById(""two"").innerHTML == 'X' && document.getElementById(""three"").innerHTML == 'X')
            document.getElementById(""message"").innerHTML = ""Player X won!"";
        else if (document.getElementById(""four"").innerHTML == 'X' && document.getElementById(""five"").innerHTML == 'X' && document.getElementById(""six"").innerHTML == 'X')
            document.getElementById(""message"").innerHTML = ""Player X won!"";
        else if (document.getElementById(""seven"").innerHTML == 'X' && document.getElementById(""eight"").innerHTML == 'X' && document.getElementById(""nine"").innerHTML == 'X')
            document.getElementById(""message"").innerHTML = ""Player X won!"";
        else if (document.getElementById(""one"").innerHTML == 'X' && document.getElementById(""four"").innerHTML == 'X' && document.getElementById(""seven"").innerHTML == 'X')
            document.getElementById(""message"").innerHTML = ""Player X won!"";
        else if (document.getElementByI");
            WriteLiteral(@"d(""two"").innerHTML == 'X' && document.getElementById(""five"").innerHTML == 'X' && document.getElementById(""eight"").innerHTML == 'X')
            document.getElementById(""message"").innerHTML = ""Player X won!"";
        else if (document.getElementById(""three"").innerHTML == 'X' && document.getElementById(""six"").innerHTML == 'X' && document.getElementById(""nine"").innerHTML == 'X')
            document.getElementById(""message"").innerHTML = ""Player X won!"";
        else if (document.getElementById(""one"").innerHTML == 'X' && document.getElementById(""five"").innerHTML == 'X' && document.getElementById(""nine"").innerHTML == 'X')
            document.getElementById(""message"").innerHTML = ""Player X won!"";
        else if (document.getElementById(""three"").innerHTML == 'X' && document.getElementById(""five"").innerHTML == 'X' && document.getElementById(""seven"").innerHTML == 'X')
            document.getElementById(""message"").innerHTML = ""Player X won!"";

        if (document.getElementById(""one"").innerHTML == 'O' && d");
            WriteLiteral(@"ocument.getElementById(""two"").innerHTML == 'O' && document.getElementById(""three"").innerHTML == 'O')
            document.getElementById(""message"").innerHTML = ""Player O won!"";
        else if (document.getElementById(""four"").innerHTML == 'O' && document.getElementById(""five"").innerHTML == 'O' && document.getElementById(""six"").innerHTML == 'O')
            document.getElementById(""message"").innerHTML = ""Player O won!"";
        else if (document.getElementById(""seven"").innerHTML == 'O' && document.getElementById(""eight"").innerHTML == 'O' && document.getElementById(""nine"").innerHTML == 'O')
            document.getElementById(""message"").innerHTML = ""Player O won!"";
        else if (document.getElementById(""one"").innerHTML == 'O' && document.getElementById(""four"").innerHTML == 'O' && document.getElementById(""seven"").innerHTML == 'O')
            document.getElementById(""message"").innerHTML = ""Player O won!"";
        else if (document.getElementById(""two"").innerHTML == 'O' && document.getElementById(""five");
            WriteLiteral(@""").innerHTML == 'O' && document.getElementById(""eight"").innerHTML == 'O')
            document.getElementById(""message"").innerHTML = ""Player O won!"";
        else if (document.getElementById(""three"").innerHTML == 'O' && document.getElementById(""six"").innerHTML == 'O' && document.getElementById(""nine"").innerHTML == 'O')
            document.getElementById(""message"").innerHTML = ""Player O won!"";
        else if (document.getElementById(""one"").innerHTML =='O' && document.getElementById(""five"").innerHTML == 'O' && document.getElementById(""nine"").innerHTML == 'O')
            document.getElementById(""message"").innerHTML = ""Player O won!"";
        else if (document.getElementById(""three"").innerHTML == 'O' && document.getElementById(""five"").innerHTML == 'O' && document.getElementById(""seven"").innerHTML == 'O')
            document.getElementById(""message"").innerHTML = ""Player O won!"";
    }
</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TicMvc.ViewModels.GameViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
