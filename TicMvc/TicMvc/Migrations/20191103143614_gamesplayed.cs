﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TicMvc.Migrations
{
    public partial class gamesplayed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GamesPlayer",
                table: "Player");

            migrationBuilder.AddColumn<int>(
                name: "GamesPlayed",
                table: "Player",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GamesPlayed",
                table: "Player");

            migrationBuilder.AddColumn<int>(
                name: "GamesPlayer",
                table: "Player",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
