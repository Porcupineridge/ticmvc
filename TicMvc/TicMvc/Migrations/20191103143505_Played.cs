﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TicMvc.Migrations
{
    public partial class Played : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GamesPlayer",
                table: "Player",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GamesPlayer",
                table: "Player");
        }
    }
}
