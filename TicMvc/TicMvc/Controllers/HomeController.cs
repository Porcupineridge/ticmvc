﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TicMvc.Models;
using TicMvc.ViewModels;

namespace TicMvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IPlayerRepository _playerRepository;
        

        public HomeController(ILogger<HomeController> logger, IPlayerRepository playerRepository)
        {
            _logger = logger;
            _playerRepository = playerRepository;
            
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Game()
        {
            GameViewModel gameViewModel = new GameViewModel()
            {
                Player = _playerRepository.GetPlayer(1),
                Opponent = _playerRepository.GetPlayer(2),
                Game = _playerRepository.GetGame(1)
                
                
            };

            return View(gameViewModel);
        }
        public IActionResult Lounge()
        {
            

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
